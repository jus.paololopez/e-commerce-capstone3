import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';
import { Redirect, useHistory } from 'react-router-dom';


export default function AddProduct() {

const history = useHistory();
const{user, setUser} = useContext(UserContext);


const [name, setName] = useState('');
const [description, setDescription] = useState('');
const [price, setPrice] = useState(0);
const [isActive, setIsActive] = useState(true);


const [addButton, setAddButton] = useState(false);


useEffect(() => {
    if(name!=='' && description!=='' && price!=='' && isActive){
        setAddButton(true)
    }else{
        setAddButton(false)
    }
}, [name, description, price, isActive])





function addProduct(e){
		e.preventDefault();
        

    fetch('http://secret-tundra-31885.herokuapp.com/products/add', {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${user.accessToken}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name: name,
                    description: description,
                    price: price,
                    isActive: isActive
                })
            })
                .then(res=>res.json())
                .then(data=>{
                console.log(data)

                if(data !==null){
                     Swal.fire({
                        //the properties of this created object will describe the structure of the alert message box
                        title: "Congrats!",
                        icon: "success",
                        text: `You have successfully added ${name}`
                        //this properties are predefined by the dependency of sweet alert
                    });
                    	history.push('/products');	

                        
                }else{
                    Swal.fire({
                        //the properties of this created object will describe the structure of the alert message box
                        title: "Product Creation failed!",
                        icon: "error",
                        text: `Failed to add this Item. Check your inputs ${name}`
                        //this properties are predefined by the dependency of sweet alert
                    });
                    }       
             
   })
          //clearout form
                        setName('');
                        setDescription('');
                        setPrice(0);
                        setIsActive(true);

}


   return(
<>
    <Form onSubmit={(e)=> addProduct(e)}>
    <h2>Create Product</h2>
	<Form.Group controlId="productName">
			<Form.Label>Product Name:</Form.Label>
			<Form.Control type="text" 
            placeholder="Enter the Name of the Card" 
            value={name} 
            onChange={e => setName(e.target.value)} required/>
		<Form.Text className="text-muted">
				Add a new Graphic Card.
			</Form.Text>
		</Form.Group>

        <Form.Group controlId="productDes">
			<Form.Label>Details:</Form.Label>
			<Form.Control 
            type="text" 
            placeholder="Enter Details of the Card" 
            value={description} 
            onChange={e => setDescription(e.target.value)} required/>
		</Form.Group>

<Form.Group controlId="price">
			<Form.Label>Price:</Form.Label>
			<Form.Control 
            type="number" 
            placeholder="Enter The Price of the Card" 
            value={price} 
            onChange={e => setPrice(e.target.value)} required/>
		</Form.Group>

        <Form.Group controlId="isActive">
			<Form.Label>On Stock</Form.Label>
			<Form.Control 
            type="text" 
            placeholder="Enter 'true' if this Card is Available" 
            value={isActive} 
            onChange={e => setIsActive(e.target.value)} required/>
		</Form.Group>


        
        {addButton ?
         <Button variant="primary" type="submit">Submit</Button> 
		:
		<Button variant="primary" type="submit" disabled>Submit</Button>
	}

        </Form>
        </>
)
}



