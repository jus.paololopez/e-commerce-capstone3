import React, { useState, useEffect, useContext } from 'react'
import { Table, Jumbotron } from 'react-bootstrap';
import Product from '../components/Product';

import UserContext from '../UserContext';


export default function MyOrders({product,userId}) {

    
const { user } = useContext(UserContext);
	console.log(user)

const [myOrders, setMyOrders] = useState([]);




useEffect(() => {



    fetch(`https://secret-tundra-31885.herokuapp.com/users/${userId}/myOrder`,{
        headers: {
           'Authorization' : `Bearer ${user.accessToken}`
        }   
    })
    .then(res => res.json())
			.then(data => {
				console.log(data)
		
		
     	setMyOrders(data.order.map(product => {
		 console.log(product)
		   
		 product.map(orders => {
			 console.log(orders)
			 return (
						 
						<tr>
					 		<td>{orders._id}</td>
							 <td>{orders.name}</td>
							 <td>{orders.description}</td>
							 
					 	</tr>
					 )
		 })
					 
					
				}));
        
    })
          

}, [])




		return (

		<>

			<Jumbotron className="mt-5">
			
				<h1 className="text-center mb-5" ><strong>My Orders</strong></h1>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Description</th>
							<th>Price</th>
							
						</tr>
					</thead>
					<tbody>
							{myOrders}
					</tbody>
				</Table>
			</Jumbotron>
		</>

		)
	}


