import React from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';


import img3 from '../images/new/new.jpg';
import img4 from '../images/new/hot.jpg';
import img5 from '../images/new/onsale.jpg';
export default function Highlights(){
   return(

	<Row>
		<Col>
			<Card className="cardHighlight">
            <Card.Img variant="top" src= { img3 } className="img1"/>
				<Card.Body>
					<Card.Title>
						<h2 >NEW ARRIVALS</h2>
					</Card.Title>
					<Card.Text>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>

		<Col>
			<Card className="cardHighlight">
            <Card.Img variant="top" src= { img5 } className="img1"/>
				<Card.Body>
					<Card.Title>
						<h2>ON SALE NOW</h2>
					</Card.Title>
					<Card.Text>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>

		<Col>
			<Card className="cardHighlight">
            <Card.Img variant="top" src= { img4 } className="img1"/>
				<Card.Body>
					<Card.Title>
						<h2>HOT PICKS</h2>
					</Card.Title>
					<Card.Text>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>
	</Row>
   )
}