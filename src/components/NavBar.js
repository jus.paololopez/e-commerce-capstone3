import React, { Fragment, useContext } from 'react';
//import necessary components
import Navbar from 'react-bootstrap/Navbar';//export default = it can be change or placed it on any variable we want
import { Nav } from 'react-bootstrap';
import { Link, NavLink, useHistory } from 'react-router-dom';
import UserContext from '../UserContext'

import { Dropdown, DropdownButton, ButtonToolbar, ButtonGroup } from 'react-bootstrap'; 
export default function NavBar() {


//push(path) pushes a new entry onto the history stack
	const history = useHistory();

	//useContext() is a react hook used to unwrap our context. It will return the data passed as values by a provider
	const { user, setUser, unsetUser } = useContext(UserContext);
	console.log(user)


	const logout = () =>{
		unsetUser();
		setUser({ accessToken: null });
		history.push('/login')
	}

let rightNav = (user.accessToken !== null) ? // if user logged in the logout option will be visible
<Fragment>
<Nav.Link as= {NavLink} to = "/myOrder">My Orders</Nav.Link>
<Nav.Link onClick={logout}>Logout</Nav.Link>
</Fragment>
:
<Fragment>

<Nav.Link as= {NavLink} to = "/login">Login</Nav.Link>
<Nav.Link as= {NavLink} to = "/register">Register</Nav.Link>

</Fragment>

let addProduct = (user.isAdmin) ? 
<Fragment>
<Nav.Link as= {NavLink} to = "/">Home</Nav.Link>
<Nav.Link as= {NavLink} to = "/products">Products</Nav.Link>
<Nav.Link as= {NavLink} to = "/addProduct">Add Product</Nav.Link>


</Fragment>
:
<Fragment>

<Nav.Link as= {NavLink} to = "/">Home</Nav.Link>
<Nav.Link as= {NavLink} to = "/products">Graphics Card</Nav.Link>




</Fragment>




 

    return(
        <Navbar bg = "light" expand="lg">
                <Navbar.Brand as={Link} to = "/">BedRoom PC</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    
                    <Navbar.Collapse id="basic-navbar-nav">
                    
                    <Nav>
                        {addProduct}
                        
 <DropdownButton as={ButtonGroup} title="Categories" id="bg-vertical-dropdown-3" size='sm' variant="outline-secondary">
    <Dropdown.Item eventKey="1">Asus</Dropdown.Item>
    <Dropdown.Item eventKey="2">MSI</Dropdown.Item>
    <Dropdown.Item eventKey="3">Zotac</Dropdown.Item>
    <Dropdown.Item eventKey="4">Gigabyte</Dropdown.Item>
    <Dropdown.Item eventKey="5">Palit</Dropdown.Item>
  </DropdownButton>


                        </Nav>
                        <Nav className = "ml-auto">
                        {rightNav}
                        </Nav>

                    
                       
                    </Navbar.Collapse>
            </Navbar>
    )
}

