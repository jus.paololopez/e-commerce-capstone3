import React, { useState, useEffect } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router';
import img4 from '../images/new/img2.png';

import PropTypes from 'prop-types';

export default function Product({product, productId}){


const history = useHistory();
const [seats, setSeats] = useState(10); 

    const [count, setCount] = useState(0);

    const [isOpen, setIsOpen] = useState(true);

    const { name, description, price } = product;

const [allProducts, setAllProducts] = useState([]);

    useEffect(()=>{
            if(seats === 0){
            setIsOpen(false)
        }
    }, [seats]) 
   
    const order = () => {
      
        fetch("http://secret-tundra-31885.herokuapp.com/users/order", {
            method: 'POST',
				headers: { 
                    'Content-Type': 'application/json',
                    'Authorization' : `Bearer ${localStorage.getItem('accessToken')}`
				},
                body:JSON.stringify({
                    productId: productId
                })
			})
            .then(res=>res.json())
            .then(data=>{
                console.log(data)

                if(data === true){

				Swal.fire({
					title: `${name} Added to your cart`,
					icon: 'success',
					text: 'Added to Cart'
				})

				.then(()=>history.go(0))
			}else{
				Swal.fire({
                    title: 'You need to log in. ',
					icon: 'error',
					text: 'Thank you'

				})
			}
            })
    }


                


    return (

        

        <Row>
            <Col>
               
                <Card className = "cardProduct">
                 
                <Card.Body>
                <Card.Img variant="top" src= { img4 } className="productImage"/>
                     <Card.Title>{name}</Card.Title>
                            
                        <Card.Text>
                        <h3>Description: </h3>

                        <p>{description}</p>
                        
                    <h4>Price: </h4>  
                    <p>Php {price}</p>

                    <h4>Available Cards: </h4>  
                    <p>{seats}</p>   

                      <h4>Owner: </h4>  
                    <p>{count}</p>          
                    
                    </Card.Text>
                    
                   {isOpen ? <Button variant="primary" onClick={() => order(productId)}>Order</Button> 
                   : 
                   <Button variant="danger" onClick={order} disabled>Full</Button>}

                   {/* <Button variant="info" onClick={getSpecific} disabled>Quick View</Button> */}

                   
                </Card.Body>

             </Card>
            </Col>
        </Row>
    )
}



Product.propTypes = {
    //lets call on the property to check the data
    //shape() method is used to check that a prop object conforms to a specific 'shape'

    data: PropTypes.shape({
        //define the properties and their expected types.
        name : PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}