import React, { useState, useEffect, useContext } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';
import UserContext from '../UserContext';

export default function UpdateButton({productId}) {

	const history = useHistory();
	const {user, setUser} = useContext(UserContext);
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);


	//forms
	
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState(true);

	const [isOpen, setIsOpen] = useState('');

	useEffect(()=>{
		if(isActive !== '' && price !== 0){
			setIsOpen(true)
		}else{
			setIsOpen(false)
		}
	}, [isActive, price])


	function updateProduct(){
		fetch(`http://secret-tundra-31885.herokuapp.com/products/${productId}/update`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${user.accessToken}`
			},
			body: JSON.stringify({
				isActive: isActive,	
				price: price
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Product Updated',
					icon: 'success',
					text: 'Product Updated'
				})

				.then(()=>history.go(0))
			}else{
				Swal.fire({
					title: 'Item Updated',
					icon: 'success',
					title: 'Product Updated.'
				})
				.then(()=>history.go(0))
			}
		})
	}


	return(
		<>
			<Button variant="primary" onClick={handleShow}>Edit</Button>


			<Modal show={show} onHide={handleClose}>
		        <Modal.Header closeButton>
		          <Modal.Title>Update a Product</Modal.Title>
		        </Modal.Header>

		        <Modal.Body>
		        	<Form>
		        		<Form.Group>
		        			<Form.Label>Availability</Form.Label>
		        			<Form.Control 
		        				type="text"
		        				placeholder="Enter 'true' if this card available"
		        				value={isActive}
		        				onChange={(e)=> setIsActive(e.target.value)}
		        				required
		        				/>
		        		</Form.Group>

		        		
		        		<Form.Group>
		        			<Form.Label>Price:</Form.Label>
		        			<Form.Control 
		        				type="number"
		        				value={price}
		        				onChange={(e)=> setPrice(e.target.value)}
		        				required
		        				/>
		        		</Form.Group>
		        	</Form>


		        </Modal.Body>
				

		        <Modal.Footer>
		          <Button variant="secondary" onClick={handleClose}>
		            Close
		          </Button>
		         
		        { 
		        	isActive ? 
					<Button type="submit" variant="primary" onClick={updateProduct}>Save Changes</Button>
					:
					<Button type="submit" variant="primary" disabled>Save Changes</Button>
				}


		        </Modal.Footer>
		    </Modal>
		</>
		)
}
