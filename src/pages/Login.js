import { useEffect, useState, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'; // state management
import { Redirect, useHistory} from 'react-router-dom';

export default function Login() {

    const history = useHistory();
    //consume the UserContext object in the login page via useContext
    const { user, setUser} = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    //variable that will describe the state of the `login button`
    const [loginButton, setLoginButton] = useState(false);

    useEffect(() => {

        if (email !== '' && password !== '') {
            setLoginButton(true) // pag valid input the button will be clickable
        } else {
            setLoginButton(false) // else disabled button
        }
    }, [email, password]) // para hindi mag infinite loop or having an error sa console


   function Login(e){
        e.preventDefault();



        //  SYNTAX FOR FETCHING 
        fetch('http://secret-tundra-31885.herokuapp.com/users/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password

                })
            })
            .then(res => res.json()) // parse into json
            .then(data => {
                console.log(data) // to show the result 

                    
                            
                  //Lets do the response in our data once we receive it 
                if (data.accessToken !== undefined) {
                     localStorage.setItem('accessToken', data.accessToken)
                     localStorage.setItem('isAdmin', data.isAdmin)
                    setUser({
                        accessToken: data.accessToken
                    });

                    Swal.fire({
                        //the properties of this created object will describe the structure of the alert message box
                        title: "Congrats!",
                        icon: "success",
                        text: "You have successfully Logged In!"
                        //this properties are predefined by the dependency of sweet alert
                   
                        })

                

                //get user details from our token
                    fetch('http://secret-tundra-31885.herokuapp.com/users/details', {
                            headers: {
                                Authorization: `Bearer ${data.accessToken}`
                            }

                        })
                        .then(res => res.json())
                        .then(data => {
                            console.log(data)
                            
                            if (data.isAdmin === true) {
                                localStorage.setItem('email', data.email)
                                localStorage.setItem('isAdmin', data.isAdmin)

                                setUser({
                                    email: data.email,
                                    isAdmin: data.isAdmin
                                })
                                //router can we use to redirect the page to the /courses if
                                //isAdmin is true
                                history.push('/');

                            } else {
                                history.push('/');
                            }
                        })
                    }else{
                         Swal.fire({
                        //the properties of this created object will describe the structure of the alert message box
                        title: "You do not have an Admin Access",
                        icon: "error",
                        text: "Ask Admin for Help."
                        //this properties are predefined by the dependency of sweetalert
                    });
                    }

                //Clear Out
                setEmail('');
                setPassword('');
            })
    }

    if (user.accessToken !== null) {
        return <Redirect to = "/" / >
    }
    
   return(
	<Form onSubmit={(e) => Login(e)}>
		<Form.Group controlId="userEmail">
			<Form.Label>Email Address:</Form.Label>
			<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
		</Form.Group>

		<Form.Group controlId="password">
			<Form.Label>Password:</Form.Label>
			<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e => setPassword(e.target.value)} required/>
		</Form.Group>
		{loginButton ? 
			<Button variant="success" type="submit">Submit</Button>
			: 
			<Button variant="success" type="submit" disabled>Submit</Button>

		}
		
	</Form>
		)

    }