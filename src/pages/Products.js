import React,{ useContext, useEffect, useState } from 'react';
import Product from '../components/Product'; 
import UserContext from '../UserContext';
import { Table, Jumbotron } from 'react-bootstrap';

import GetProduct from '../components/GetProduct';
import UpdateButton from '../components/UpdateButton';
import DeleteButton from '../components/DeleteButton';
import admin from '../images/new/admin.png';

export default function Products(){

	
	const [adminProducts, setAdminProducts] = useState([]);
	//for normal user
	const [allProducts, setAllProducts] = useState([]);


	const { user } = useContext(UserContext);
	console.log(user)

		useEffect(() => {

			
		if(!user.isAdmin){

			fetch("https://secret-tundra-31885.herokuapp.com/products/allProduct", {
				headers: { 
					Authorization: `Bearer ${user.accessToken}`
				}
			})
			.then(response => response.json())
			.then(userProducts =>{
				console.log(userProducts);

				setAllProducts(userProducts.map(product => {
					return (

						<Product key={product._id} product={product}
						 productId={product._id}/>
						
						
					)
				}));	

			});
		

		} else if (user.isAdmin==true) {

			// no auth.verify in the backend, options {} can be removed
			fetch("https://secret-tundra-31885.herokuapp.com/products/allProduct", {
				headers: { 
					
					Authorization: `Bearer ${user.accessToken}` 
					
				}
			})
			.then(res => res.json())
			.then(activeProducts => {
				console.log(activeProducts)

				// show all data that we need for the admin
			
				setAdminProducts(activeProducts.map(product => {
					// render array of react elements into our component
					return (
						<tr key={product._id}>
							<td>{product._id}</td>
							<td>{product.name}</td>
							<td>{product.description}</td>
							<td>{product.price}</td>
							<td className={product.isActive ? "text-success" : "text-danger"}>{product.isActive ? "Active" : "Inactive"}</td>
						
						<td><UpdateButton productId={product._id} /></td>
						<td><DeleteButton productId={product._id} /></td>
						<td><GetProduct productId={product._id} /></td>
						
						</tr>
					)

				}));
			})

		}
		
		
	}, [])

	// conditional rendering for the UI that will appear if admin or not
	if (!user.isAdmin) {

		return (

			[allProducts]	
		)

	} else {

		return (

		<>

			<Jumbotron className="mt-5">
			
				<h1 className="text-center mb-5" ><strong> <img src={ admin } className="imgAdmin" />Inventory Control</strong></h1>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Description</th>
							<th>Price</th>
							<th>Status</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
							{adminProducts}
					</tbody>
				</Table>
			</Jumbotron>
		</>

		)
	}
}
