// import logo from './logo.svg';
// import Cartoon from './image.jpg'
import React, { useState } from 'react';
import './App.css';
import NavBar from './components/NavBar';
import Home from './pages/Home';
// import Banner from './components/Banner';
import { Container } from 'react-bootstrap';

//Import react router
import { BrowserRouter as Router} from 'react-router-dom';
import { Route, Switch} from 'react-router-dom'; // creating route for different component


import Register from './pages/Register';
import Login from './pages/Login';
import Products from './pages/Products';
import NotFound from './pages/NotFound'; // if invalid link
import AddProduct from './pages/addProduct';
import UserContext from './UserContext';
import MyOrders from './pages/MyOrders';

function App() {
  

const [user,setUser] = useState({ accessToken:localStorage.getItem('accessToken'), 
                                  isAdmin:localStorage.getItem('isAdmin')==='true'});


//Finally, to logout, lets update the App.js file first and unset the user
 const unsetUser = ()  =>  {
    localStorage.clear() //to clear the saved data in the localStorage
    setUser({accessToken:null, isAdmin:null})
  }


return ( 
<UserContext.Provider value = {{ user, setUser, unsetUser}}>

      <Router>
        <NavBar />
        <Container> 
            <Switch>
              <Route exact path= "/" component = {Home}  />
              <Route exact path= "/login" component = {Login}  />
              <Route exact path= "/register" component = {Register}  />
              <Route exact path= "/products" component = {Products}  />
              <Route exact path= "/addProduct" component = {AddProduct}  />
              <Route exact path= "/myOrder" component = {MyOrders}  />
              <Route component = {NotFound}  /> 
              
          </Switch>
    </Container>
  </Router>
</UserContext.Provider>


  );
}

export default App;