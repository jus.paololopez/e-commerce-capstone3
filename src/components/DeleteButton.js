import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';


// use props
export default function DeleteButton({productId}) {

    function deleteProduct(e){
        e.preventDefault();

        fetch(`http://secret-tundra-31885.herokuapp.com/products/${productId}/delete`,{
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : `Bearer ${localStorage.getItem('accessToken')}`
        }
        })
        .then(res=>res.json())
        .then(data =>{
            console.log(data);


            if(data.isActive === false){
                Swal.fire({
                    title:'successful',
                    icon: 'success',
                    text: 'Item Inactive'
                })
                .then(()=> window.location.reload())
            }else{
                 Swal.fire({
                    title:'Error',
                    icon: 'error',
                    text: 'Failed to Delete a Product'
                })
            }
        })
    }

    return (
       <Button variant="danger" onClick={deleteProduct}>InActive</Button>
    )
}
