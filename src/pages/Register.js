import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register(){
	// let's define state hooks for all input fields
	const history = useHistory();
	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isAdmin, setIsAdmin] = useState(false);
	

	//let's declare a variable that will describe the state of the register button component
	const [registerButton, setRegisterButton] = useState(false)

	//our next task, let us try to refactor our register user function
	useEffect(() =>{
		if((email !== '' && password !== '' && isAdmin !=='')
		 && (password.length >= 8)){
			setRegisterButton(true)

		}else{
			//here in the else, let's describe the return if any of the condition has not met
			setRegisterButton(false)
		}
	}, [email, password, isAdmin])


// -------------------- REGISTER-------------------------------------//

	function registerUser(e){
		//let's describe the event that will happen upon registering a new user
		e.preventDefault(); //this is to avoid page redirection 


			fetch('http://secret-tundra-31885.herokuapp.com/users/', {
				method:'POST',
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
					email:email,
					password:password,
					isAdmin:isAdmin
				})
			
			})
			.then(res => res.text()) // parse into json
            .then(data => {
					 console.log(data) // to show the result 
			
			Swal.fire({
                        //the properties of this created object will describe the structure of the alert message box
                        title: "Congrats!",
                        icon: "success",
                        text: "You have successfully registered"
                        //this properties are predefined by the dependency of sweet alert
                    });
						
					history.push('/login');	

			//clearout the data inside the input fields
			setEmail('');
			setPassword('');
			setIsAdmin('');
})
	
		}
			

	  if(user.accessToken !== null){
	  	return <Redirect to="/" />
	  }
 

	return(

	<Form onSubmit={(e)=> registerUser(e)}>

	<Form.Group controlId="email">
			<Form.Label>Email Address:</Form.Label>
			<Form.Control type="email" placeholder="Enter Email Address" value={email} onChange={e => setEmail(e.target.value)} required/>
		<Form.Text className="text-muted">
				We'll never share your information with anyone else.
			</Form.Text>
		</Form.Group>


		<Form.Group controlId="password1">
			<Form.Label>Password:</Form.Label>
			<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e => setPassword(e.target.value)} required/>
		</Form.Group>

<Form.Group controlId="set">
			<Form.Label>Type true if you are an Admin:</Form.Label>
			<Form.Control type="text" placeholder="Type 'true' if you are an Admin" value={isAdmin} onChange={e => setIsAdmin(e.target.value)} required/>
		</Form.Group>

		
		{registerButton ? <Button variant="primary" type="submit">Submit</Button> 
		:
		<Button variant="primary" type="submit" disabled>Submit</Button>
	}
		
		
	</Form>
		)

}
